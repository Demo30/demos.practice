﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.ComponentModel;
using Demos.Practice.PracticeStrategies;

namespace Demos.Practice
{
    public class PracticeManager
    {
        public bool PracticeActivated
        { 
            get
            {
                return
                    this.Practice == null ?
                        false :
                        this.Practice.PracticeState == PracticeStrategies.Enumerations.PracticeStates.ACTIVE ? true : false;
            }
        }
        public IPractice Practice { get; private set; }

        public PracticeManager ()
        {

        }

        public void SetPractice(IPractice strategy)
        {
            if (strategy != null)
            {
                this.ClearCurrentPracticeStrategy();
                this.Practice = strategy;
            } else
            {
                throw new ArgumentNullException();
            }
            
        }

        public void ClearCurrentPracticeStrategy()
        {
            if (this.Practice != null)
            {
                if (this.Practice.PracticeState != PracticeStrategies.Enumerations.PracticeStates.ACTIVE)
                {
                    this.Practice.PracticeStateChanged -= this.OnPracticeStrategyStateChanged;
                    this.Practice = null;
                } else
                {
                    throw new Exception("Current strategy is active. Cannot clear it.");
                }
            } else
            {
                return;
            }
        }

        protected void OnPracticeStrategyStateChanged(object sender, PracticeStateChangedEventArgs args)
        {
            // hmm
        }
        
    }
}
