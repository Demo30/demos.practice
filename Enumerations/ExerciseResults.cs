﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demos.Practice
{
    public enum ExerciseResults
    {
        CORRECTLY_ANSWERED,
        INCORRECTLY_ANSWERD,
        NO_ANSWER_AVAILABLE,
        NOT_EVALUATED
    }
}
