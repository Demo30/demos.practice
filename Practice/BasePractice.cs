﻿using Demos.Practice.PracticeStrategies.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demos.Practice
{
    abstract public class BasePractice<Q> : IPractice where Q:Question
    {
        public event EventHandler<PracticeStateChangedEventArgs> PracticeStateChanged = null;
        public PracticeStates PracticeState { get; private set; } = PracticeStates.INACTIVE;
        
        public ExerciseServer ExerciseServer { get; set; }

        public BasePractice(ExerciseServer exerciseServer)
        {
            this.ExerciseServer = exerciseServer;
        }

        protected void ChangePracticeState(PracticeStates state)
        {
            if (state != this.PracticeState)
            {
                this.PracticeState = state;
                this.RaisePracticeStatusChangedEvent(new PracticeStateChangedEventArgs(this));
            }
        }

        private void RaisePracticeStatusChangedEvent(PracticeStateChangedEventArgs args)
        {
            if (this.PracticeStateChanged != null)
            {
                this.PracticeStateChanged(this, args);
            }
        }
    }
}
