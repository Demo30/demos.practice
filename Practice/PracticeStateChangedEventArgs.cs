﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demos.Practice
{
    public class PracticeStateChangedEventArgs : EventArgs
    {
        public IPractice Practice { get { return this._practice; } }

        private IPractice _practice = null;

        public PracticeStateChangedEventArgs(IPractice practice)
        {
            this._practice = practice;
        }

    }
}
