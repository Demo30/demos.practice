﻿using Demos.Practice.PracticeStrategies.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demos.Practice
{
    public interface IPractice
    {
        event EventHandler<PracticeStateChangedEventArgs> PracticeStateChanged;
        PracticeStates PracticeState { get; }
    }
}
