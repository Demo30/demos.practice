﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demos.Practice
{
    public class PracticeTimed<Q> : BasePractice<Q> where Q:Question
    {
        public ExerciseTimedProviderServer<Q> ExerciseServerTimed
        { 
            get { return (ExerciseTimedProviderServer<Q>)this.ExerciseServer; }
        }

        public PracticeTimed(ExerciseTimedProviderServer<Q> server) : base(server)
        {
            this.PracticeStateChanged += PracticeTimed_PracticeStateChanged;
        }

        public void StartPractice()
        {
            this.ChangePracticeState(PracticeStrategies.Enumerations.PracticeStates.ACTIVE);
        }

        public void StopPractice()
        {
            this.ChangePracticeState(PracticeStrategies.Enumerations.PracticeStates.INACTIVE);
        }

        private void PracticeTimed_PracticeStateChanged(object sender, PracticeStateChangedEventArgs e)
        {
            if (this.PracticeState == PracticeStrategies.Enumerations.PracticeStates.ACTIVE)
            {
                this.ExerciseServerTimed.StartTimer();
            }
            else if (this.PracticeState == PracticeStrategies.Enumerations.PracticeStates.INACTIVE)
            {
                this.ExerciseServerTimed.StopTimer();
            }
        }
    }
}
