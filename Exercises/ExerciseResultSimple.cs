﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demos.Practice
{
    public class ExerciseResultSimple : ExerciseResult
    {
        public override ExerciseResults Result { get; }

        public ExerciseResultSimple(ExerciseResults result)
        {
            this.Result = result;
        }
    }
}
