﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demos.Practice
{
    public abstract class Exercise<Q> : IExercise where Q: Question
    {
        public delegate bool AnswerCompatibilityCheck(Answer answer);

        public abstract bool MultipleAnswersAllowed { get; }

        public abstract IExerciseEvaluator ExerciseEvaluator { get; }

        protected abstract AnswerCompatibilityCheck GetCheckAnswerCompatibility();

        public bool Active { get { return this._active; } }

        public Question GetAbstractQuestion()
        {
            return this.GetQuestion();
        }

        public Q GetQuestion()
        {
            return this._question;
        }

        public Answer[] GetAnswers()
        {
            return this._answers.ToArray();
        }

        public ExerciseResult GetResult()
        {
            if (this.GetAnswers().Length == 0)
            {
                return new ExerciseResultSimple(ExerciseResults.NO_ANSWER_AVAILABLE);
            }
            else
            {
                if (this.ExerciseEvaluator != null)
                {
                    return this.ExerciseEvaluator.EvaluateExercise();
                } else
                {
                    throw new Exception("Exercise evaluator not available.");
                }
            }
        }

        private List<Answer> _answers = new List<Answer>();
        private Q _question = null;
        private bool _active = true;

        public Exercise(Q question)
        {
            if (question != null)
            {
                this._question = question;
            } else
            {
                throw new ArgumentNullException();
            }
        }

        public void AddAnswer(Answer answer)
        {
            // Checking state

            Question question = this.GetQuestion();
            AnswerCompatibilityCheck answerCompatibilityCheck = this.GetCheckAnswerCompatibility();

            if (question == null)
            {
                throw new Exception("Missing question object.");
            }

            if (answerCompatibilityCheck == null)
            {
                throw new Exception("Missing answer compatibility check.");
            }

            if (answer == null)
            {
                throw new ArgumentNullException();
            }

            if (!answer.Submitted)
            {
                throw new AnswerNotSubmittedException();
            }

            // Adding answer

            if (this._answers.Count == 0 || this.MultipleAnswersAllowed)
            {
                if (answerCompatibilityCheck(answer))
                {
                    this._answers.Add(answer);
                }
                else
                {
                    throw new ArgumentException("Provided answer is incompatible with current question.");
                }
            }
            else
            {
                throw new ArgumentException("Multiple answers not allowed.");
            }
        }

        public void Deactivate()
        {
            this._active = false;
        }

    }
}
