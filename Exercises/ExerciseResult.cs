﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demos.Practice
{
    public abstract class ExerciseResult
    {
        public abstract ExerciseResults Result { get; }
    }
}
