﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demos.Practice
{
    public abstract class Answer
    {
        public bool Submitted { get { return this._submitted; } }

        private bool _submitted = false;

        public void SubmitAnswer()
        {
            this._submitted = true;
        }
    }
}
