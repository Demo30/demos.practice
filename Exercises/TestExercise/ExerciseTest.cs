﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demos.Practice
{
    public class ExerciseTest : 
        Exercise<QuestionTest>,
        IExerciseEvaluator
    {
        protected override AnswerCompatibilityCheck GetCheckAnswerCompatibility()
        {
            return (Answer Answer) =>
            {
                return Answer is Answer;
            };
        }

        public ExerciseResult EvaluateExercise()
        {
            Answer[] answers = this.GetAnswers();
            foreach (AnswerTest answer in answers)
            {
                if (answer.Result == 5)
                {
                    return new ExerciseResultSimple(ExerciseResults.CORRECTLY_ANSWERED);
                }
            }
            return new ExerciseResultSimple(ExerciseResults.INCORRECTLY_ANSWERD);
        }

        public override bool MultipleAnswersAllowed { get { return false; } }

        public override IExerciseEvaluator ExerciseEvaluator { get { return this; } }

        public ExerciseTest(QuestionTest question) : base(question)
        {

        }
    }
}
