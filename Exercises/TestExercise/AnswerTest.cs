﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demos.Practice
{
    public class AnswerTest : Answer
    {
        public int Result { get; private set; }

        public AnswerTest(int answer)
        {
            this.Result = answer;
        }
    }
}
