﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demos.Practice
{
    public interface IExercise
    { 
        bool MultipleAnswersAllowed { get; } // All this answer business will probably get further abstracted as well

        bool Active { get; }

        IExerciseEvaluator ExerciseEvaluator { get; }

        Question GetAbstractQuestion();

        Answer[] GetAnswers();

        ExerciseResult GetResult();

        void AddAnswer(Answer answer);

        void Deactivate();
    }
}
