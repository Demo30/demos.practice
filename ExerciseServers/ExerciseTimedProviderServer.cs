﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Demos.Practice
{
    public class ExerciseTimedProviderServer<Q> : ExerciseProviderServer<Q> where Q : Question
    {
        public event EventHandler<EventArgs> Tick;

        public override int BufferCapacity
        {
            get { return this.bufferCapacity; }
        }
        public int ExerciseServingFrequencyInMs { get; set; } = 4000;
        public int TimePassedInMs { get { return this._ticksPassed * this.TickingPeriodInMs; } }
        public int TicksInExerecisePeriod { get { return (int)(this.ExerciseServingFrequencyInMs / this.TickingPeriodInMs); } }
        public int TicksRemainingInExercisePeriod { get { return TicksPassed % TicksInExerecisePeriod; } }
        public int TickingPeriodInMs { get; } = 50;
        public int TicksPassed { get { return this._ticksPassed; } }
        
        private int bufferCapacity = 0;
        private int _ticksPassed = 0;
        private Timer _ticker = null;
        private Timer _bufferFiller;

        public ExerciseTimedProviderServer(IExerciseProvider<Q> provider) : base (provider)
        {

        }

        public void StartTimer()
        {
            if (this._ticker == null)
            {
                this._ticker = new Timer(this.OnTick, null, 0, this.TickingPeriodInMs);
            }
            else
            {
                this._ticker.Change(0, this.TickingPeriodInMs);
            }
            this.StartBufferTimer();
        }

        public void StopTimer()
        {
            if (this._ticker != null)
            {
                this._ticker.Change(Timeout.Infinite, Timeout.Infinite);
                this._ticker = null;
            }
            this.StopBufferTimer();
        }

        public void SetBufferCapacity(int bufferCapacity)
        {
            if (bufferCapacity >= 0)
            {
                this.bufferCapacity = bufferCapacity;
            } else
            {
                throw new ArgumentOutOfRangeException();
            }
        }

        private void StartBufferTimer()
        {
            int bufferPeriod = 1000;
            if (this._bufferFiller == null)
            {
                this._bufferFiller = new Timer(this.OnBufferTick, null, 0, bufferPeriod);
            }
            else
            {
                this._bufferFiller.Change(0, bufferPeriod);
            }
        }

        private void StopBufferTimer()
        {
            if (this._bufferFiller != null)
            {
                this._bufferFiller.Change(Timeout.Infinite, Timeout.Infinite);
                this._bufferFiller = null;
            }
        }

        private void OnTick(object state)
        {
            this._ticksPassed++;
            if (this.CurrentExercise == null || this.TicksRemainingInExercisePeriod == 0)
            {
                this.ServeExercise(this.GetNewExercise());
            }
            if (this.Tick != null)
            {
                this.Tick(this, EventArgs.Empty);
            }
        }

        private void OnBufferTick(object state)
        {
            if (!this.BufferFillerLock)
            {
                this.FillBuffer();
            }
        }

    }
}
