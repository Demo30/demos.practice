﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demos.Practice
{
    public class ExerciseManualProviderServer<Q> : ExerciseProviderServer<Q> where Q : Question
    {
        public override int BufferCapacity { get { return this._bufferSize; } }

        private int _bufferSize = 0;

        public ExerciseManualProviderServer(IExerciseProvider<Q> provider) : base (provider)
        {

        }

        public void ServeNextExercise()
        {
            this.ServeExercise(this.GetNewExercise());
        }
    }
}
