﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demos.Practice
{
    public abstract class ExerciseProviderServer<Q> : ExerciseServer where Q : Question
    {
        public IExerciseProvider<Q> Provider { get; }

        protected bool BufferFillerLock { get; private set; }

        public ExerciseProviderServer(IExerciseProvider<Q> exerciseProvider)
        {
            if (exerciseProvider != null)
            {
                this.Provider = exerciseProvider;
            }
            else
            {
                throw new ArgumentNullException();
            }
        }

        public Exercise<Q> GetNewExercise()
        {
            if (this._buffer.Count > 0)
            {
                return (Exercise<Q>)this._buffer.Dequeue();
            }
            else
            {
                return this.Provider.CreateExercise();
            }
        }

        protected override void FillBuffer()
        {
            if (!this.BufferFillerLock)
            {
                int currentBufferSize = this._buffer.Count;
                double flex = 0.75;
                if (currentBufferSize < (int)(this.BufferCapacity * flex))
                {
                    this.BufferFillerLock = true;
                    try
                    {
                        List<IExercise> buffer = new List<IExercise>();
                        int toFill = this.BufferCapacity - currentBufferSize;
                        while (buffer.Count < toFill)
                        {
                            IExercise exercise = this.Provider.CreateExercise();
                            buffer.Add(exercise);
                        }
                        IExercise[] bufferArray = buffer.ToArray();
                        foreach (IExercise exercise in bufferArray)
                        {
                            this._buffer.Enqueue(exercise);
                        }
                    }
                    finally
                    {
                        this.BufferFillerLock = false;
                    }
                }
            }
            else
            {
                throw new InvalidOperationException("Buffer filler is currently locked.");
            }
        }

    }
}
