﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demos.Practice
{
    public abstract class ExerciseServer
    {
        public event EventHandler<EventArgs> ExerciseArchived;
        public event EventHandler<EventArgs> ExerciseServed;

        public IExercise CurrentExercise { get { return this._currentExercise; } }

        public IExercise[] Archive { get { return this._archive.ToArray(); } }
        public IExercise[] Buffer { get { return this._buffer.ToArray(); } }

        public IExerciseEvaluator DefaultExerciseEvaluator { get; set; }

        public abstract int BufferCapacity { get; }

        protected List<IExercise> _archive = new List<IExercise>();
        protected Queue<IExercise> _buffer = new Queue<IExercise>();
        private IExercise _currentExercise = null;

        public ExerciseServer()
        {

        }

        protected void ArchiveCurrentExercise()
        {
            if (this.CurrentExercise != null)
            {
                this._archive.Add(this.CurrentExercise);
                if (this.CurrentExercise.Active)
                {
                    this.CurrentExercise.Deactivate();
                }
                this._currentExercise = null;
                this.RaiseExerciseArchivedEvent();
            }
        }

        protected void ServeExercise(IExercise exercise)
        {
            if (exercise.Active)
            {
                // Setup new exercise
                if (this.CurrentExercise != null)
                {
                    this.ArchiveCurrentExercise();
                }
                this._currentExercise = exercise;

                // Set default exercise evaluator
                if (this.DefaultExerciseEvaluator != null)
                {
                    if (exercise is IExerciseCustomizableEvaluator exerciseCustomizable)
                    {
                        exerciseCustomizable.SetExerciseEvaluator(this.DefaultExerciseEvaluator);
                    }
                    else
                    {
                        throw new InvalidOperationException("Default exercise evaluator is supplied, but served exercise does not support custom setup.");
                    }
                }

                // Raise notification
                this.RaiseExerciseServedEvent();
            } else
            {
                throw new ArgumentException("Cannot serve inactive exercise.");
            }
        }

        protected abstract void FillBuffer();

        private void RaiseExerciseArchivedEvent()
        {
            if (this.ExerciseArchived != null)
            {
                this.ExerciseArchived(this, new EventArgs());
            }
        }

        private void RaiseExerciseServedEvent()
        {
            if (this.ExerciseServed != null)
            {
                this.ExerciseServed(this, new EventArgs());
            }
        }

    }
}
